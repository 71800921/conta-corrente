package br.com.cotemig.conta.corrente;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
    static ArrayList<Conta> contas = new ArrayList<>();
    static Scanner entrada = new Scanner(System.in);
    static ArrayList<Integer> numerosGerados = new ArrayList<>();
    static int numeroConta;
    static Random numero = new Random();
    static double valorInicial;
    static String nome;

    public static void main(String[] args) {

        int opcao;

        do {
            exibeMenu();
            opcao = entrada.nextInt();
            escolheOpcao(opcao);
        } while (opcao != 4);
    }

    public static void exibeMenu() {
        System.out.println("\t Escolha a opção desejada");
        System.out.println("1 - Já Possuo uma conta");
        System.out.println("2 - Desejo abrir uma nova conta");
        System.out.println("3 - Sair\n");
    }

    public static void escolheOpcao(int opcao) {
        double valor;

        switch (opcao) {
            case 1:
                System.out.print("Informe o número da conta: ");
                numeroConta = entrada.nextInt();

                Conta contaEncontrada = buscarConta(numeroConta);
                if (contaEncontrada != null) {
                    contaEncontrada.iniciar();
                }
            case 2:
                System.out.println("Cadastrando novo cliente.");
                System.out.print("Entre com o seu nome: ");
                nome = entrada.next();

                System.out.print("Entre com o valor inicial depositado na conta: ");
                valorInicial = entrada.nextDouble();
                numeroConta = gerarNumeroConta();

                Conta novaConta = new Conta(nome, numeroConta, valorInicial);
                contas.add(novaConta);
                novaConta.iniciar();
            case 3:
                System.out.println("Sistema encerrado.");
                break;

            default:
                System.out.println("Opção inválida");
        }
    }

    public static Conta buscarConta(int numeroConta) {
        for (int posicaoContas = 0; posicaoContas < contas.size(); posicaoContas++) {
            if (contas.get(posicaoContas).getConta() == numeroConta) {
                return contas.get(posicaoContas);
            }
        }
        return null;
    }

    public static int gerarNumeroConta() {
        int numeroGerado = 0;
        do {
            numeroGerado = 1 + numero.nextInt(9999);
            if (numerosGerados.contains(numeroGerado)) {
                numeroGerado = 1 + numero.nextInt(9999);
            }
        } while (numeroGerado == 0);
        numerosGerados.add(numeroGerado);
        return numeroGerado;
    }


}
